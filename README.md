# Make base box
```
$ vagrant up
$ vagrant package --output devp.box

# before destroy, copy private_key
$ cp .vagrant/machines/default/virtualbox/private_key ./
$ vagrant destroy
```

If You're Behind a Proxy Server, proxyconf plugin should be needed and you have to pass proxy env, `VAGRANT_HTTP_PROXY`, `VAGRANT_HTTPS_PROXY` when all `vagrant up` command.

```
$ vagrant plugin install vagrant-proxyconf
$ VAGRANT_HTTP_PROXY=http://id:pass@1.2.3.4:8080/ VAGRANT_HTTPS_PROXY=http://id:pass@1.2.3.4:8080/ vagrant up
```

## artifacts
- box file
- key file
