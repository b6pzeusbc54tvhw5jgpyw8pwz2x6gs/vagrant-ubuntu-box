#!/bin/bash

# Docker installation should be done before come here
# https://github.com/tmatilai/vagrant-proxyconf/issues/155
sudo usermod -aG docker ubuntu

# install base programs
sudo apt-get install apache2-utils git heirloom-mailx -y
curl -sL https://bootstrap.pypa.io/get-pip.py | sudo -H python3 -
sudo -H pip install docker-compose
sudo -H pip install awscli

# pre-pull docker image
# sudo docker pull traefik:1.4.2

# default user settings
# https://www.vagrantup.com/docs/boxes/base.html
# 개어렵네
pwd
sudo mkdir -p /home/vagrant/.ssh
curl https://www.vagrantup.com/docs/boxes/base.html >> tmp_keys
sudo cp tmp_keys /home/vagrant/.ssh/authorized_keys
sudo chmod 700 /home/vagrant/.ssh
sudo chmod 600 /home/vagrant/.ssh/authorized_keys

# cleaning histories
sudo apt-get autoremove -y
sudo apt-get clean
sudo dd if=/dev/zero of=/EMPTY bs=1M
sudo rm -f /EMPTY
sudo cat /dev/null > ~/.bash_history && history -c
